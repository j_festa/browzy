﻿using Browzy.Model.Customers;
using Browzy.Model.Repository;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Browzy.Tests
{
    public class RepositoryBucket
    {
        public List<ShippingSlip> CreatedShippingSlips { get; set; } = new List<ShippingSlip>();

        public List<Tuple<Customer, string>> ActivatedMemberships { get; set; } = new List<Tuple<Customer, string>>();

        public IRepository Repository { get; set; }
        public Mock<IRepository> Mock { get; set; }
    }
}
