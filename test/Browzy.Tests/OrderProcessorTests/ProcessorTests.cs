﻿using Browzy.Model.Customers;
using Browzy.Model.Store;
using Browzy.OrderProcessor;
using Browzy.OrderProcessor.BusinessRules;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Browzy.Tests.OrderProcessorTests
{
    public class ProcessorTests
    {
        [Fact]
        public void EmptyRuleListThrowsException()
        {
            var order = new PurchaseOrder()
            {
                Id = 1232,
                CustomerId = TestItems.Customer2.Id,
                TotalPrice = TestItems.Book1984.Price,
                Lines = new List<Item>() { TestItems.Book1984 }
            };
            var processor = new Processor();

            Assert.Throws<InvalidOperationException>(() => processor.Execute(order));
        }

        [Fact]
        public void ListWithOneElementDoesntFail()
        {
            var order = new PurchaseOrder()
            {
                Id = 1233,
                CustomerId = TestItems.Customer2.Id,
                TotalPrice = TestItems.Book1984.Price,
                Lines = new List<Item>() { TestItems.Book1984 }
            };

            var processor = new Processor();
            var repositoryBucket = TestItems.GetRepository();

            processor.Add(TestItems.GetEmptyBusinessRule(), 1);
            processor.Execute(order);

            Assert.Empty(repositoryBucket.ActivatedMemberships);
            Assert.Empty(repositoryBucket.CreatedShippingSlips);
        }

        [Fact]
        public void ShippingSlipGeneratedWithBothRules()
        {
            var order = new PurchaseOrder()
            {
                Id = 1235,
                CustomerId = TestItems.Customer2.Id,
                TotalPrice = TestItems.Book1984.Price,
                Lines = new List<Item>() { TestItems.Book1984 }
            };

            var processor = new Processor();
            var repositoryBucket = TestItems.GetRepository();

            processor.Add(new BusinessRule1(repositoryBucket.Repository), 1);
            processor.Add(new BusinessRule2(repositoryBucket.Repository), 2);
            processor.Execute(order);

            Assert.Empty(repositoryBucket.ActivatedMemberships);
            Assert.Single(repositoryBucket.CreatedShippingSlips);
            repositoryBucket.Mock.Verify(n => n.ShippingSlipAdd(It.IsAny<ShippingSlip>()), Times.Once);
            repositoryBucket.Mock.Verify(n => n.ActivateMembership(It.IsAny<Customer>(), It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public void ShippingSlipGeneratedWithBothRulesReverseOrder()
        {
            var order = new PurchaseOrder()
            {
                Id = 1235,
                CustomerId = TestItems.Customer2.Id,
                TotalPrice = TestItems.Book1984.Price,
                Lines = new List<Item>() { TestItems.Book1984 }
            };

            var processor = new Processor();
            var repositoryBucket = TestItems.GetRepository();

            processor.Add(new BusinessRule2(repositoryBucket.Repository), 1);
            processor.Add(new BusinessRule1(repositoryBucket.Repository), 2);
            processor.Execute(order);

            Assert.Empty(repositoryBucket.ActivatedMemberships);
            Assert.Single(repositoryBucket.CreatedShippingSlips);
            repositoryBucket.Mock.Verify(n => n.ShippingSlipAdd(It.IsAny<ShippingSlip>()), Times.Once);
            repositoryBucket.Mock.Verify(n => n.ActivateMembership(It.IsAny<Customer>(), It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public void MembershipActivatedAndShippingSlipGenerated()
        {
            var order = new PurchaseOrder()
            {
                Id = 1236,
                CustomerId = TestItems.Customer2.Id,
                TotalPrice = 125.0m,
                Lines = new List<Item>() 
                { 
                    TestItems.Book1984, 
                    TestItems.VideoFirstAid, 
                    TestItems.BookGirl, 
                    TestItems.MembershipBook,
                    TestItems.MembershipBillieEilish
                }
            };

            var processor = new Processor();
            var repositoryBucket = TestItems.GetRepository();

            processor.Add(new BusinessRule1(repositoryBucket.Repository), 1);
            processor.Add(new BusinessRule2(repositoryBucket.Repository), 2);
            processor.Execute(order);

            Assert.Single(repositoryBucket.ActivatedMemberships);
            Assert.Single(repositoryBucket.CreatedShippingSlips);
            repositoryBucket.Mock.Verify(n => n.ShippingSlipAdd(It.IsAny<ShippingSlip>()), Times.Once);
            repositoryBucket.Mock.Verify(n => n.ActivateMembership(It.IsAny<Customer>(), It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public void MembershipActivatedAndShippingSlipGeneratedReverseOrder()
        {
            var order = new PurchaseOrder()
            {
                Id = 1236,
                CustomerId = TestItems.Customer2.Id,
                TotalPrice = 125.0m,
                Lines = new List<Item>()
                {
                    TestItems.Book1984,
                    TestItems.VideoFirstAid,
                    TestItems.BookGirl,
                    TestItems.MembershipBook,
                    TestItems.MembershipBillieEilish
                }
            };

            var processor = new Processor();
            var repositoryBucket = TestItems.GetRepository();

            processor.Add(new BusinessRule2(repositoryBucket.Repository), 1);
            processor.Add(new BusinessRule1(repositoryBucket.Repository), 2);
            processor.Execute(order);

            Assert.Single(repositoryBucket.ActivatedMemberships);
            Assert.Single(repositoryBucket.CreatedShippingSlips);
            repositoryBucket.Mock.Verify(n => n.ShippingSlipAdd(It.IsAny<ShippingSlip>()), Times.Once);
            repositoryBucket.Mock.Verify(n => n.ActivateMembership(It.IsAny<Customer>(), It.IsAny<string>()), Times.Once);
        }
    }
}
