using Browzy.Model.Customers;
using Browzy.Model.Store;
using Browzy.OrderProcessor.BusinessRules;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Browzy.Tests.OrderProcessorTests.BusinessRulesTests
{
    public class Br1Tests
    {
        [Fact]
        public void OrderWithoutMemberships()
        {
            var order = new PurchaseOrder()
            {
                Id = 1234,
                CustomerId = TestItems.Customer1.Id,
                TotalPrice = TestItems.Book1984.Price,
                Lines = new List<Item>() { TestItems.Book1984 }
            };
            var repositoryBucket = TestItems.GetRepository();
            var rule = new BusinessRule1(repositoryBucket.Repository);

            rule.Apply(order);

            Assert.Empty(repositoryBucket.ActivatedMemberships);
            repositoryBucket.Mock.Verify(n => n.ActivateMembership(It.IsAny<Customer>(), It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public void OrderWithoutItems()
        {
            var order = new PurchaseOrder()
            {
                Id = 1234,
                CustomerId = TestItems.Customer1.Id,
                TotalPrice = TestItems.Book1984.Price,
                Lines = new List<Item>()
            };
            var repositoryBucket = TestItems.GetRepository();
            var rule = new BusinessRule1(repositoryBucket.Repository);

            rule.Apply(order);

            Assert.Empty(repositoryBucket.ActivatedMemberships);
            repositoryBucket.Mock.Verify(n => n.ActivateMembership(It.IsAny<Customer>(), It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public void OrderWithOneMembership()
        {
            var order = new PurchaseOrder()
            {
                Id = 1234,
                CustomerId = TestItems.Customer1.Id,
                TotalPrice = TestItems.MembershipBook.Price,
                Lines = new List<Item>() { TestItems.MembershipBook }
            };
            var repositoryBucket = TestItems.GetRepository();
            var rule = new BusinessRule1(repositoryBucket.Repository);

            rule.Apply(order);

            Assert.Single(repositoryBucket.ActivatedMemberships);
            repositoryBucket.Mock.Verify(n => n.ActivateMembership(It.IsAny<Customer>(), It.IsAny<string>()), Times.Once);

            Assert.Collection(repositoryBucket.ActivatedMemberships,
                (item) => Assert.Equal(item.Item2, TestItems.MembershipBook.MembershipId));
        }

        [Fact]
        public void OrderWithOneMembershipAndProducts()
        {
            var order = new PurchaseOrder()
            {
                Id = 1234,
                CustomerId = TestItems.Customer1.Id,
                TotalPrice = TestItems.MembershipBook.Price,
                Lines = new List<Item>() { TestItems.MembershipBook, TestItems.BookGirl, TestItems.VideoFirstAid }
            };
            var repositoryBucket = TestItems.GetRepository();
            var rule = new BusinessRule1(repositoryBucket.Repository);

            rule.Apply(order);

            Assert.Single(repositoryBucket.ActivatedMemberships);
            repositoryBucket.Mock.Verify(n => n.ActivateMembership(It.IsAny<Customer>(), It.IsAny<string>()), Times.Once);

            Assert.Collection(repositoryBucket.ActivatedMemberships,
                (item) => Assert.Equal(item.Item2, TestItems.MembershipBook.MembershipId));
        }

        [Fact]
        public void OrderWithOneMembershipAlreadyExisting()
        {
            var order = new PurchaseOrder()
            {
                Id = 1234,
                CustomerId = TestItems.Customer2.Id,
                TotalPrice = TestItems.MembershipBook.Price,
                Lines = new List<Item>() { TestItems.MembershipBook }
            };
            var repositoryBucket = TestItems.GetRepository();
            var rule = new BusinessRule1(repositoryBucket.Repository);

            rule.Apply(order);

            Assert.Empty(repositoryBucket.ActivatedMemberships);
            repositoryBucket.Mock.Verify(n => n.ActivateMembership(It.IsAny<Customer>(), It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public void OrderWithMoreMemberships()
        {
            var order = new PurchaseOrder()
            {
                Id = 1234,
                CustomerId = TestItems.Customer1.Id,
                TotalPrice = TestItems.MembershipBook.Price,
                Lines = new List<Item>() { TestItems.MembershipBook, TestItems.MembershipBillieEilish }
            };
            var repositoryBucket = TestItems.GetRepository();
            var rule = new BusinessRule1(repositoryBucket.Repository);

            rule.Apply(order);

            Assert.Equal<int>(2, repositoryBucket.ActivatedMemberships.Count());
            repositoryBucket.Mock.Verify(n => n.ActivateMembership(It.IsAny<Customer>(), It.IsAny<string>()), Times.Exactly(2));

            Assert.Contains(repositoryBucket.ActivatedMemberships, 
                (s) => s.Item2.Equals(TestItems.MembershipBook.MembershipId) && s.Item1.Id == TestItems.Customer1.Id);
            Assert.Contains(repositoryBucket.ActivatedMemberships, 
                (s) => s.Item2.Equals(TestItems.MembershipBillieEilish.MembershipId) && s.Item1.Id == TestItems.Customer1.Id);
        }

        [Fact]
        public void OrderWithTwoMembershipOneAlreadyExisting()
        {
            var order = new PurchaseOrder()
            {
                Id = 1234,
                CustomerId = TestItems.Customer2.Id,
                TotalPrice = TestItems.MembershipBook.Price,
                Lines = new List<Item>() { TestItems.MembershipBook, TestItems.MembershipBillieEilish }
            };
            var repositoryBucket = TestItems.GetRepository();
            var rule = new BusinessRule1(repositoryBucket.Repository);

            rule.Apply(order);

            Assert.Single(repositoryBucket.ActivatedMemberships);
            repositoryBucket.Mock.Verify(n => n.ActivateMembership(It.IsAny<Customer>(), It.IsAny<string>()), Times.Once);

            Assert.Collection(repositoryBucket.ActivatedMemberships,
                (item) => Assert.Equal(item.Item2, TestItems.MembershipBillieEilish.MembershipId));
        }
    }
}
