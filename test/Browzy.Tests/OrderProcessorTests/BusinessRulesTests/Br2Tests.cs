using Browzy.Model.Customers;
using Browzy.Model.Store;
using Browzy.OrderProcessor.BusinessRules;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Browzy.Tests.OrderProcessorTests.BusinessRulesTests
{
    public class Br2Tests
    {
        [Fact]
        public void OrderWithoutItems()
        {
            var order = new PurchaseOrder()
            {
                Id = 1234,
                CustomerId = TestItems.Customer1.Id,
                TotalPrice = 0.0m,
                Lines = new List<Item>()
            };
            var repositoryBucket = TestItems.GetRepository();
            var rule = new BusinessRule2(repositoryBucket.Repository);

            rule.Apply(order);

            Assert.Empty(repositoryBucket.CreatedShippingSlips);
            repositoryBucket.Mock.Verify(n => n.ShippingSlipAdd(It.IsAny<ShippingSlip>()), Times.Never);
        }

        [Fact]
        public void OrderWithoutProducts()
        {
            var order = new PurchaseOrder()
            {
                Id = 1234,
                CustomerId = TestItems.Customer1.Id,
                TotalPrice = TestItems.MembershipBillieEilish.Price,
                Lines = new List<Item>() { TestItems.MembershipBillieEilish }
            };
            var repositoryBucket = TestItems.GetRepository();
            var rule = new BusinessRule2(repositoryBucket.Repository);

            rule.Apply(order);

            Assert.Empty(repositoryBucket.CreatedShippingSlips);
            repositoryBucket.Mock.Verify(n => n.ShippingSlipAdd(It.IsAny<ShippingSlip>()), Times.Never);
        }

        [Fact]
        public void OrderWithoutPhysicalProducts()
        {
            var order = new PurchaseOrder()
            {
                Id = 1234,
                CustomerId = TestItems.Customer1.Id,
                TotalPrice = TestItems.MembershipBillieEilish.Price + TestItems.VideoFirstAid.Price,
                Lines = new List<Item>() { TestItems.MembershipBillieEilish, TestItems.VideoFirstAid }
            };
            var repositoryBucket = TestItems.GetRepository();
            var rule = new BusinessRule2(repositoryBucket.Repository);

            rule.Apply(order);

            Assert.Empty(repositoryBucket.CreatedShippingSlips);
            repositoryBucket.Mock.Verify(n => n.ShippingSlipAdd(It.IsAny<ShippingSlip>()), Times.Never);
        }

        [Fact]
        public void OrderWithOnePhysicalProduct()
        {
            var order = new PurchaseOrder()
            {
                Id = 1234,
                CustomerId = TestItems.Customer1.Id,
                TotalPrice = TestItems.Book1984.Price,
                Lines = new List<Item>() { TestItems.Book1984 }
            };
            var repositoryBucket = TestItems.GetRepository();
            var rule = new BusinessRule2(repositoryBucket.Repository);

            rule.Apply(order);

            Assert.Single(repositoryBucket.CreatedShippingSlips);
            repositoryBucket.Mock.Verify(n => n.ShippingSlipAdd(It.IsAny<ShippingSlip>()), Times.Once);

            var shippingSlip = repositoryBucket.CreatedShippingSlips.Single();
            Assert.Equal(TestItems.Customer1.Id, shippingSlip.CustomerId);
            Assert.Equal(TestItems.Customer1.GetHeader(), shippingSlip.RecipientName);
            Assert.Equal(TestItems.Customer1.Address, shippingSlip.Address);
            Assert.Single(shippingSlip.Lines);
            var line = shippingSlip.Lines.Single();
            Assert.Equal(TestItems.Book1984.Price, line.Price);
            Assert.Equal(TestItems.Book1984.Name, line.ProductName);
        }

        [Fact]
        public void OrderWithMorePhysicalProducts()
        {
            var order = new PurchaseOrder()
            {
                Id = 1234,
                CustomerId = TestItems.Customer1.Id,
                TotalPrice = TestItems.Book1984.Price + TestItems.BookGirl.Price + TestItems.MembershipBook.Price,
                Lines = new List<Item>() { TestItems.Book1984, TestItems.BookGirl, TestItems.MembershipBook }
            };
            var repositoryBucket = TestItems.GetRepository();
            var rule = new BusinessRule2(repositoryBucket.Repository);

            rule.Apply(order);

            Assert.Single(repositoryBucket.CreatedShippingSlips);
            repositoryBucket.Mock.Verify(n => n.ShippingSlipAdd(It.IsAny<ShippingSlip>()), Times.Once);

            var shippingSlip = repositoryBucket.CreatedShippingSlips.Single();
            Assert.Equal(TestItems.Customer1.Id, shippingSlip.CustomerId);
            Assert.Equal(TestItems.Customer1.GetHeader(), shippingSlip.RecipientName);
            Assert.Equal(TestItems.Customer1.Address, shippingSlip.Address);
            Assert.Equal(2, shippingSlip.Lines.Count());
            Assert.Contains(shippingSlip.Lines,
                (s) => s.ProductName == TestItems.Book1984.Name && s.Price == TestItems.Book1984.Price);
            Assert.Contains(shippingSlip.Lines,
                (s) => s.ProductName == TestItems.BookGirl.Name && s.Price == TestItems.BookGirl.Price);
        }
    }
}
