﻿using Browzy.Model.Customers;
using Browzy.Model.Repository;
using Browzy.Model.Store;
using Browzy.OrderProcessor;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Browzy.Tests
{
    public static class TestItems
    {
        public static ProductType ProductTypeBook = new ProductType() { Name = "Book", IsPhysical = true };
        public static ProductType ProductTypeVideo = new ProductType() { Name = "Video", IsPhysical = false };
        public static ProductType ProductTypeCandy = new ProductType() { Name = "Candy", IsPhysical = true };

        public static Product Book1984 = new Product()
        {
            Name = "1984",
            Price = 9.99m,
            Type = ProductTypeBook
        };

        public static Product BookGirl = new Product()
        {
            Name = "The Girl on the train",
            Price = 6.99m,
            Type = ProductTypeBook
        };

        public static Product VideoFirstAid = new Product()
        {
            Name = "Comprehensive First Aid Training",
            Price = 12.99m,
            Type = ProductTypeVideo
        };

        public static MembershipRequest MembershipBook = new MembershipRequest()
        {
            Name = "Book Club Membership",
            Price = 16.00m,
            MembershipId = "BOOK_CLUB"
        };

        public static MembershipRequest MembershipBillieEilish = new MembershipRequest()
        {
            Name = "Billie Eilish Fan Club Membership",
            Price = 16.00m,
            MembershipId = "BE_FAN_CLUB"
        };

        public static Customer Customer1 = new Customer()
        {
            Id = 100,
            Name = "John",
            Surname = "Doe",
            Address = "123 Fake Street",
            Email = "john.doe@test.com",
        };

        public static Customer Customer2 = new Customer()
        {
            Id = 101,
            Name = "Chad",
            Surname = "Virgin",
            Address = "123 Frog Street",
            Email = "chad@test.com",
            Memberships = new HashSet<string>(new[] { MembershipBook.MembershipId })
        };

        public static Customer Customer3 = new Customer()
        {
            Id = 102,
            Name = "Jack",
            Surname = "Nicholson",
            Address = "123 Actor Street",
            Email = "mad@test.com",
            Memberships = new HashSet<string>(new[] { MembershipBook.MembershipId })
        };

        public static Dictionary<long, Customer> Customers = new Dictionary<long, Customer>()
        {
            { Customer1.Id, Customer1 },
            { Customer2.Id, Customer2 },
            { Customer3.Id, Customer3 },
        };

        public static RepositoryBucket GetRepository()
        {
            var bucket = new RepositoryBucket();

            var mock = new Mock<IRepository>();
            mock.Setup(m => m.ActivateMembership(It.IsAny<Customer>(), It.IsAny<string>()))
                .Callback((Customer c, string m) => bucket.ActivatedMemberships.Add(new Tuple<Customer, string>(c, m)));
            mock.Setup(m => m.ShippingSlipAdd(It.IsAny<ShippingSlip>()))
                .Callback((ShippingSlip ss) => bucket.CreatedShippingSlips.Add(ss));
            mock.Setup(m => m.GetCustomerById(It.IsAny<long>()))
                .Returns((long customerId) => Customers[customerId].Clone() as Customer);

            bucket.Mock = mock;
            bucket.Repository = mock.Object;

            return bucket;
        }

        public static IBusinessRule GetEmptyBusinessRule()
        {
            var mock = new Mock<IBusinessRule>();
            return mock.Object;
        }
    }
}
