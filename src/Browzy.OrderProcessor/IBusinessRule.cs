﻿using Browzy.Model.Repository;
using Browzy.Model.Store;
using System;
using System.Collections.Generic;
using System.Text;

namespace Browzy.OrderProcessor
{
    public interface IBusinessRule
    {
        void Apply(PurchaseOrder order);
    }
}
