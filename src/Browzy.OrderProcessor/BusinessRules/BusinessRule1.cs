﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Browzy.Model.Repository;
using Browzy.Model.Store;

namespace Browzy.OrderProcessor.BusinessRules
{
    public class BusinessRule1 : BusinessRule
    {
        public BusinessRule1(IRepository repository) : base(repository)
        {

        }

        public override void Apply(PurchaseOrder order)
        {
            //select all the order memberships
            var membershipRequests = order.Lines.Select(l => l as MembershipRequest)
                .Where(mr => mr != null);

            //BR doesn't apply if there are no memberships
            if (!membershipRequests.Any())
                return;

            //get the customer: needed to check already existing memberships
            var customer = Repository.GetCustomerById(order.CustomerId);

            //activation
            foreach (var request in membershipRequests)
            {
                if (!customer.Memberships.Contains(request.MembershipId))
                {
                    Repository.ActivateMembership(customer, request.MembershipId);
                }
            }
        }
    }
}
