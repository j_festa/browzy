﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Browzy.Model.Customers;
using Browzy.Model.Repository;
using Browzy.Model.Store;

namespace Browzy.OrderProcessor.BusinessRules
{
    public class BusinessRule2 : BusinessRule
    {
        public BusinessRule2(IRepository repository) : base(repository)
        {

        }

        public override void Apply(PurchaseOrder order)
        {
            //select all the physical products
            var physicalProducts = order.Lines.Select(l => l as Product)
                .Where(p => p != null && p.Type.IsPhysical);

            //BR doesn't apply if there are no eligible products
            if (!physicalProducts.Any())
                return;

            //get the customer: needed populate the ShippingSlip
            var customer = Repository.GetCustomerById(order.CustomerId);

            //generate the shipping slip
            var shippingSlip = new ShippingSlip()
            {
                CustomerId = customer.Id,
                RecipientName = customer.GetHeader(),
                Address = customer.Address,
                Lines = physicalProducts.Select(p => new ShippingSlipLine()
                {
                    ProductName = p.Name,
                    Price = p.Price
                }).ToList()
            };

            Repository.ShippingSlipAdd(shippingSlip);
        }
    }
}
