﻿using Browzy.Model.Repository;
using Browzy.Model.Store;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Browzy.OrderProcessor
{
    /// <summary>
    /// Flexible processor supporting a priority queue of business rules. Each BR can be added with an assigned priority.
    /// BRs are executed from the lowest to the higher priority value. If two BRs have the same priority,
    /// the natural ordering will apply (i.e. the first added BR will be executed first).
    /// </summary>
    public class Processor
    {
        private List<BrItem> _rules;

        public Processor()
        {
            _rules = new List<BrItem>();
        }

        /// <summary>
        /// Creates a Processor with pre-assigned BRs; Each rule gets assigned a incremental priority according to their
        /// IEnumerable order.
        /// </summary>
        /// <param name="rules">Rules to add</param>
        public Processor(IEnumerable<IBusinessRule> rules)
        {
            int i = 0;
            _rules = new List<BrItem>(
                rules.Select(br => new BrItem()
                    {
                        Priority = ++i,
                        BusinessRule = br
                    }));
        }

        public void Add(IBusinessRule rule, int priority)
        {
            _rules.Add(new BrItem() 
                { 
                    BusinessRule = rule, 
                    Priority = priority 
                });
        }

        public void Execute(PurchaseOrder order)
        {
            if (!_rules.Any())
                throw new InvalidOperationException("No BR attached");

            foreach (var rule in _rules
                .OrderBy(r => r.Priority)
                .Select(r => r.BusinessRule))
            {
                rule.Apply(order);
            }
        }

        private class BrItem
        {
            public int Priority { get; set; }
            public IBusinessRule BusinessRule { get; set; }
        }
    }
}
