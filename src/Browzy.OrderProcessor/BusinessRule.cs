﻿using Browzy.Model.Repository;
using Browzy.Model.Store;
using System;
using System.Collections.Generic;
using System.Text;

namespace Browzy.OrderProcessor
{
    public abstract class BusinessRule : IBusinessRule
    {
        protected IRepository Repository { get; set; }

        public BusinessRule(IRepository repository)
        {
            Repository = repository;
        }

        public abstract void Apply(PurchaseOrder order);
    }
}
