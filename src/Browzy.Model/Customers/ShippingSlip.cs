﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Browzy.Model.Customers
{
    public class ShippingSlip
    {
        public long CustomerId { get; set; }
        public string RecipientName { get; set; }
        public string Address { get; set; }
        public List<ShippingSlipLine> Lines { get; set; }
    }
}
