﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Browzy.Model.Customers
{
    public class Customer : ICloneable
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }

        public HashSet<string> Memberships { get; set; } = new HashSet<string>();

        public string GetHeader()
        {
            return (!String.IsNullOrWhiteSpace(Name) && !String.IsNullOrWhiteSpace(Surname))
                ? String.Concat(Name, " ", Surname)
                : (!String.IsNullOrWhiteSpace(Name) ? Name : Surname); 
        }

        public object Clone()
        {
            return new Customer()
            {
                Id = Id,
                Name = Name,
                Surname = Surname,
                Address = Address,
                Email = Email,
                Memberships = new HashSet<string>(Memberships)
            };
        }
    }
}
