﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Browzy.Model.Customers
{
    public class ShippingSlipLine
    {
        public string ProductName { get; set; }
        public decimal Price { get; set; }
    }
}
