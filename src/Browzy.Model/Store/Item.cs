﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Browzy.Model.Store
{
    public abstract class Item
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
    }
}
