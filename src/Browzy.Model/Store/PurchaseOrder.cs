﻿using System;
using System.Collections.Generic;

namespace Browzy.Model.Store
{
    public class PurchaseOrder
    {
        public long Id { get; set; }
        public long CustomerId { get; set; }
        public decimal TotalPrice { get; set; }
        public List<Item> Lines { get; set; }
    }
}
