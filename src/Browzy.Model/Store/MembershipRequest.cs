﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Browzy.Model.Store
{
    public class MembershipRequest : Item
    {
        public string MembershipId { get; set; }
    }
}
