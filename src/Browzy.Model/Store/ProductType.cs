﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Browzy.Model.Store
{
    public class ProductType
    {
        public string Name { get; set; }
        public bool IsPhysical { get; set; }
    }
}
