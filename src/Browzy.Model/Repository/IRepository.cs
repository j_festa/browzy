﻿using Browzy.Model.Customers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Browzy.Model.Repository
{
    public interface IRepository
    {
        Customer GetCustomerById(long customerId);
        void ActivateMembership(Customer customer, string membershipId);
        void ShippingSlipAdd(ShippingSlip item);
    }
}
